!------------------------------------------------------------------------
!! subroutine computeStressTriangleStrainVolumePlaneStrainTanhSinh
!! computes the stress field associated with triangle strain using the
!! double-exponential quadrature, following
!!
!!   Barbot S., Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume, submitted to Bull. Seism. Soc. Am., 2018.
!!
!! and considering the following geometry:
!!
!!              surface
!!      -------------+-------------- E (x2)
!!                   |
!!                   |     + A
!!                   |    /  . 
!!                   |   /     .  
!!                   |  /        .            
!!                   | /           .      
!!                   |/              + B
!!                   /            .
!!                  /|          /  
!!                 / :       .
!!                /  |    /
!!               /   : .
!!              /   /|
!!             / .   :
!!            +      |
!!          C        :
!!                   |
!!                   D (x3)
!!
!!
!! INPUT:
!! x2, x3             east coordinates and depth of the observation point,
!! A, B, C            coordinates of the 3 vertices A = ( xA, zA),
!! eij                source strain component 22, 23 and 33 in the shear zone,
!! G                  rigidity in the half space.
!! nu                 Poisson's ratio the half space.
!!
!! OUTPUT:
!! s22                horizontal stress,
!! s23                shear stress,
!! s33                vertical stress.
!!
!! \author Sylvain Barbot (03/12/18) - original Fortran form
!-----------------------------------------------------------------------
SUBROUTINE computeStressTriangleStrainVolumePlaneStrainTanhSinh( &
                x2,x3,A,B,C,e22,e23,e33,G,nu,s22,s23,s33)

  IMPLICIT NONE

  REAL*8, INTENT(IN) :: x2,x3
  REAL*8, DIMENSION(2), INTENT(IN) :: A,B,C
  REAL*8, INTENT(IN) :: e22,e23,e33
  REAL*8, INTENT(IN) :: G,nu
  REAL*8, INTENT(OUT) :: s22,s23,s33
    
  REAL*8, PARAMETER :: PI = 3.141592653589793115997963468544185161_8

  ! Heaviside function
  REAL*8, EXTERNAL :: heaviside

  ! normal vectors
  REAL*8, DIMENSION(2) :: nA, nB, nC

  ! moment density
  REAL*8 :: m22,m23,m33

  ! Lame parameter
  REAL*8 :: lambda

  ! displacement gradients
  REAL*8 :: U22, U23, U32, U33

  ! displacement offsets
  REAL*8 :: u2p2,u2p3,u2m2,u2m3
  REAL*8 :: u3p2,u3p3,u3m2,u3m3

  ! position
  REAL*8 :: omega

  ! dimension
  REAL*8 :: l, step

  ! check valid parameters
  IF ((-1._8 .GT. nu) .OR. (0.5_8 .LT. nu)) THEN
     WRITE (0,'("error: -1<=nu<=0.5, nu=",ES9.2E2," given.")') nu
     STOP 1
  END IF

  IF (0 .GT. x3) THEN
     WRITE (0,'("error: observation depth (x3) must be positive")')
     STOP 1
  END IF

  IF ((0 .GT. A(2)) .OR. &
      (0 .GT. B(2)) .OR. &
      (0 .GT. C(2))) THEN
     WRITE (0,'("error: vertex depth must be positive")')
     STOP 1
  END IF

  ! unit vectors
  nA = (/ C(2)-B(2), &
          B(1)-C(1) /) / NORM2(C-B)
  nB = (/ C(2)-A(2), &
          A(1)-C(1) /) / NORM2(C-A)
  nC = (/ B(2)-A(2), &
          A(1)-B(1) /) / NORM2(B-A)
  
  ! check that unit vectors are pointing outward
  IF (DOT_PRODUCT(nA,A-(B+C)/2) .GT. 0) THEN
      nA=-nA
  END IF
  IF (DOT_PRODUCT(nB,B-(A+C)/2) .GT. 0) THEN
      nB=-nB
  END IF
  IF (DOT_PRODUCT(nC,C-(A+B)/2) .GT. 0) THEN
      nC=-nC
  END IF

  ! Lame parameter
  lambda=2*G*nu/(1.d0-2.d0*nu)

  ! strain components, numerical solution
  U22=0.d0
  U32=0.d0
  U23=0.d0
  U33=0.d0
  DO k=-n,n
     wk=(0.5*h*PI*COSH(k*h))/(COSH(0.5*PI*SINH(k*h)))**2
     xk=TANH(0.5*PI*SINH(k*h))

     U22=U22+wk*IU22(xk)
     U32=U32+wk*IU32(xk)
     U23=U23+wk*IU23(xk)
     U33=U33+wk*IU33(xk)

  END DO

  ! remove anelastic strain
  omega=heaviside(((A(1)+B(1))/2-x2)*nC(1)+((A(2)+B(2))/2-x3)*nC(2)) &
       *heaviside(((B(1)+C(1))/2-x2)*nA(1)+((B(2)+C(2))/2-x3)*nA(2)) &
       *heaviside(((C(1)+A(1))/2-x2)*nB(1)+((C(2)+A(2))/2-x3)*nB(2))

  U22=U22-omega*e22
  U23=U23-omega*e23
  U32=U32-omega*e23
  U33=U33-omega*e33

  ! stress components
  s22=lambda*(U22+U33)+2*G*U22
  s23=G*(U23+U32)
  s33=lambda*(U22+U33)+2*G*U33

CONTAINS

  ! linear combinations of Green's functions
  REAL*8 FUNCTION IU22(t)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: t

    IU22=(m22*nC(1)+m23*nC(2))*norm(B-A)/2*G222(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m23*nC(1)+m33*nC(2))*norm(B-A)/2*G322(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m22*nA(1)+m23*nA(2))*norm(C-B)/2*G222(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m23*nA(1)+m33*nA(2))*norm(C-B)/2*G322(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m22*nB(1)+m23*nB(2))*norm(A-C)/2*G222(y(t,C(1),A(1)),y(t,C(2),A(2))) &
        +(m23*nB(1)+m33*nB(2))*norm(A-C)/2*G322(y(t,C(1),A(1)),y(t,C(2),A(2)))

  END FUNCTION IU22

  REAL*8 FUNCTION IU23(t)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: t

    IU23=(m22*nC(1)+m23*nC(2))*norm(B-A)/2*G223(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m23*nC(1)+m33*nC(2))*norm(B-A)/2*G323(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m22*nA(1)+m23*nA(2))*norm(C-B)/2*G223(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m23*nA(1)+m33*nA(2))*norm(C-B)/2*G323(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m22*nB(1)+m23*nB(2))*norm(A-C)/2*G223(y(t,C(1),A(1)),y(t,C(2),A(2))) &
        +(m23*nB(1)+m33*nB(2))*norm(A-C)/2*G323(y(t,C(1),A(1)),y(t,C(2),A(2)))

  END FUNCTION IU23

  REAL*8 FUNCTION IU32(t)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: t

    IU32=(m22*nC(1)+m23*nC(2))*norm(B-A)/2*G232(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m23*nC(1)+m33*nC(2))*norm(B-A)/2*G332(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m22*nA(1)+m23*nA(2))*norm(C-B)/2*G232(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m23*nA(1)+m33*nA(2))*norm(C-B)/2*G332(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m22*nB(1)+m23*nB(2))*norm(A-C)/2*G232(y(t,C(1),A(1)),y(t,C(2),A(2))) &
        +(m23*nB(1)+m33*nB(2))*norm(A-C)/2*G332(y(t,C(1),A(1)),y(t,C(2),A(2)))

  END FUNCTION IU32

  REAL*8 FUNCTION IU33(t)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: t

    IU33=(m22*nC(1)+m23*nC(2))*norm(B-A)/2*G233(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m23*nC(1)+m33*nC(2))*norm(B-A)/2*G333(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m22*nA(1)+m23*nA(2))*norm(C-B)/2*G233(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m23*nA(1)+m33*nA(2))*norm(C-B)/2*G333(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m22*nB(1)+m23*nB(2))*norm(A-C)/2*G233(y(t,C(1),A(1)),y(t,C(2),A(2))) &
        +(m23*nB(1)+m33*nB(2))*norm(A-C)/2*G333(y(t,C(1),A(1)),y(t,C(2),A(2)))

  END FUNCTION IU33

  ! interpolate from a to b
  REAL*8 FUNCTION y(t,a,b)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: t,a,b

    y=(a+b)/2+t*(b-a)/2

  END FUNCTION y

  !-----------------------------------------------------------------
  !> function G222
  !! Green's function G222 for an elastic half space in plane strain
  !! corresponding to the displacement gradient U22 due to a line
  !! force in the x2 direction.
  !-----------------------------------------------------------------
  REAL*8 FUNCTION G222(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G222=-1.d0/(2*pi*(1.d0-nu))*( &
            1.d0/4.d0*(3.d0-4*nu)*(x2-y2)/r1(y2,y3)**2 &
           +1.d0/4.d0*(8.d0*nu**2-12.d0*nu+5.d0)*(x2-y2)/r2(y2,y3)**2 &
           -1.d0/2.d0*(x3-y3)**2/r1(y2,y3)**4 *(x2-y2) &
           -1.d0/2.d0*((3.d0-4.d0*nu)*(x3+y3)**2+2*y3*(x3+y3)-2*y3**2)/r2(y2,y3)**4 *(x2-y2) &
           +4.d0*y3*x3*(x3+y3)**2/r2(y2,y3)**6*(x2-y2) &
         )

  END FUNCTION G222

  !---------------------------------------------------------------
  !> function G223
  !! Green's function G223 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G223(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G223= -1/(2*pi*(1-nu))*( &
     1/4*(3-4*nu)*(x3-y3)/r1(y2,y3)**2 &
    +1/4*(8*nu^2-12*nu+5)*(x3+y3)/r2(y2,y3)**2 &
    +1/2*(x3-y3)/r1(y2,y3)**2 &
    -1/2*(x3-y3)**2/r1(y2,y3)**4*(x3-y3) &
    +1/2*((3-4*nu)*(x3+y3)+y3)/r2(y2,y3)**2 &
    -1/2*((3-4*nu)*(x3+y3)**2+2*y3*(x3+y3)-2*y3**2)/r2(y2,y3)**4*(x3+y3) &
    -y3*(x3+y3)**2/r2(y2,y3)**4 &
    -2*y3*x3*(x3+y3)/r2(y2,y3)**4 &
    +4*y3*x3*(x3+y3)**2/r2(y2,y3)**6*(x3+y3))

  END FUNCTION G223

  !---------------------------------------------------------------
  !> function G232
  !! Green's function G232 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G232(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G23d2= 1/(2*pi*(1-nu))*( &
    (1-2*nu)*(1-nu)*(x3+y3)/r2(y2,y3)**2 &
    +1/4*(x3-y3)/r1(y2,y3)**2 &
    -1/2*(x3-y3)*(x2-y2)/r1(y2,y3)**4 *(x2-y2) &
    +1/4*(3-4*nu)*(x3-y3)/r2(y2,y3)**2 &
    -1/2*(3-4*nu)*(x3-y3)*(x2-y2)/r2(y2,y3)**4*(x2-y2) &
    -y3*x3*(x3+y3)/r2(y2,y3)**4 &
    +2*y3*x3*(x2-y2)*(x3+y3)/r2(y2,y3)**6*(2*x2-2*y2) &
    )

  END FUNCTION G232

  !---------------------------------------------------------------
  !> function G233
  !! Green's function G233 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G233(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G233=@(y2,y3) 1/(2*pi*(1-nu))*( &
    -(1-2*nu)*(1-nu)*(x2-y2)/r2(y2,y3)**2 &
    +1/4*(x2-y2)/r1(y2,y3)**2 &
    -1/2*(x3-y3)*(x2-y2)/r1(y2,y3)**4*(x3-y3) &
    +1/4*(3-4*nu)*(x2-y2)/r2(y2,y3)**2 &
    -1/2*(3-4*nu)*(x3-y3)*(x2-y2)/r2(y2,y3)**4*(x3+y3) &
    -y3*(x2-y2)*(x3+y3)/r2(y2,y3)**4 &
    -y3*x3*(x2-y2)/r2(y2,y3)**4 &
    +4*y3*x3*(x2-y2)*(x3+y3)/r2(y2,y3)**6*(x3+y3) &
    )

  END FUNCTION G233

  !---------------------------------------------------------------
  !> function G322
  !! Green's function G322 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G322(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G322= 1/(2*pi*(1-nu))*( &
    -(1-2*nu)*(1-nu)*(x3+y3)/r2(y2,y3)**2 &
    +1/4*(x3-y3)/r1(y2,y3)**2 &
    -1/2*(x3-y3)*(x2-y2)/r1(y2,y3)**4*(x2-y2) &
    +1/4*(3-4*nu)*(x3-y3)/r2(y2,y3)**2 &
    -1/2*(3-4*nu)*(x3-y3)*(x2-y2)/r2(y2,y3)**4*(x2-y2) &
    +y3*x3*(x3+y3)/r2(y2,y3)**4 &
    -4*y3*x3*(x2-y2)*(x3+y3)/r2(y2,y3)**6*(x2-y2) &
    )

  END FUNCTION G322

  !---------------------------------------------------------------
  !> function G323
  !! Green's function G323 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G323(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G323=@(y2,y3) 1/(2*pi*(1-nu))*( &
    (1-2*nu)*(1-nu)*(x2-y2)/r2(y2,y3)**2 &
    +1/4*(x2-y2)/r1(y2,y3)**2 &
    -1/2*(x3-y3)*(x2-y2)/r1(y2,y3)**4*(x3-y3) &
    +1/4*(3-4*nu)*(x2-y2)/r2(y2,y3)**2 &
    -1/2*(3-4*nu)*(x3-y3)*(x2-y2)/r2(y2,y3)**4*(x3+y3) &
    +y3*(x2-y2)*(x3+y3)/r2(y2,y3)**4 &
    +y3*x3*(x2-y2)/r2(y2,y3)**4 &
    -4*y3*x3*(x2-y2)*(x3+y3)/r2(y2,y3)**6*(x3+y3) &
    )

  END FUNCTION G323

  !---------------------------------------------------------------
  !> function G332
  !! Green's function G332 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G332(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G332= 1/(2*pi*(1-nu))*( &
    -1/4*(3-4*nu)*(x2-y2)/r1(y2,y3)**2 &
    -1/4*(8*nu^2-12*nu+5)*(x2-y2)/r2(y2,y3)**2 &
    -1/2*(x2-y2)/r1(y2,y3)**2 &
    +1/2*(x2-y2)**2/r1(y2,y3)**4*(x2-y2) &
    -1/2*(3-4*nu)*(x2-y2)/r2(y2,y3)**2 &
    -1/2*(0*2*y2*x3-(3-4*nu)*(x2-y2)**2)/r2(y2,y3)**4*(x2-y2) &  ! writing 0* reduces the residuals by 8 orders of magnitude
    -3/2*2*y3*x3*(x2-y2)/r2(y2,y3)**4 &                          ! writing 3* reduces the residuals by 1 order of magnitude
    +4*y3*x3*(x2-y2)**2/r2(y2,y3)**6*(x2-y2) &
    )

  END FUNCTION G332

  !---------------------------------------------------------------
  !> function G333
  !! Green's function G333 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G333(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G333=@(y2,y3) 1/(2*pi*(1-nu))*( &
    -1/4*(3-4*nu)*(x3-y3)/r1(y2,y3)**2 &
    -1/4*(8*nu**2-12*nu+5)*(x3+y3)/r2(y2,y3)**2 &
    +1/2*(x2-y2)**2/r1(y2,y3)**4*(x3-y3) &
    +1/2*y2/r2(y2,y3)**2 &
    -1/2*(2*y2*x3-(3-4*nu)*(x2-y2)**2)/r2(y2,y3)**4*(x3+y3) &
    -y3*(x2-y2)**2/r2(y2,y3)**4 &
    +4*y3*x3*(x2-y2)**2/r2(y2,y3)**6*(x3+y3) &
    )

  END FUNCTION G333

  ! Radii
  REAL*8 FUNCTION r1(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    r1=SQRT((x2-y2)**2+(x3-y3)**2)

  END FUNCTION r1

  REAL*8 FUNCTION r2(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    r2=SQRT((x2-y2)**2+(x3+y3)**2)

  END FUNCTION r2

END SUBROUTINE computeStressTriangleStrainVolumePlaneStrainTanhSinh



