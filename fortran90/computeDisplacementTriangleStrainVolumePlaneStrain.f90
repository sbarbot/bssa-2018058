!------------------------------------------------------------------------
!! subroutine computeDisplacementTriangleStrainVolumePlaneStrain computes
!! the displacement field associated with triangle strain volumes 
!! using the analytic solution of
!!
!!   Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume", submitted to Bull. Seism. Soc. Am., 2018.
!!
!! and considering the following geometry:
!!
!!              surface
!!      -------------+-------------- E (x2)
!!                   |
!!                   |     + A
!!                   |    /  . 
!!                   |   /     .  
!!                   |  /        .            
!!                   | /           .      
!!                   |/              + B
!!                   /            .
!!                  /|          /  
!!                 / :       .
!!                /  |    /
!!               /   : .
!!              /   /|
!!             / .   :
!!            +      |
!!          C        :
!!                   |
!!                   D (x3)
!!
!!
!! INPUT:
!! x2, x3             east coordinates and depth of the observation point,
!! A, B, C            coordinates of the 3 vertices A = ( xA, zA),
!! eij                source strain component 22, 23 and 33 in the shear zone,
!! nu                 Poisson's ratio the half space.
!!
!! OUTPUT:
!! u2                 displacement component in the east direction,
!! u3                 displacement component in the depth direction.
!!
!! \author Sylvain Barbot (03/12/18) - original Fortran form
!-----------------------------------------------------------------------
SUBROUTINE computeDisplacementTriangleStrainVolumePlaneStrain(x2,x3,A,B,C, &
                              e22,e23,e33,nu,u2,u3)

  IMPLICIT NONE

  REAL*8, INTENT(IN) :: x2,x3
  REAL*8, DIMENSION(2), INTENT(IN) :: A,B,C
  REAL*8, INTENT(IN) :: e22,e23,e33
  REAL*8, INTENT(IN) :: nu
  REAL*8, INTENT(OUT) :: u2,u3
    
  REAL*8, PARAMETER :: PI = 3.141592653589793115997963468544185161_8

#define LOG(X) REAL(log(DCMPLX(X,0._8)))

  ! normal vectors
  REAL*8, DIMENSION(2) :: nA, nB, nC

  ! trace
  REAL*8 :: ekk

  ! moment density
  REAL*8 :: m22,m23,m33

  ! Lame parameter
  REAL*8 :: lambda

  ! check valid parameters
  IF ((-1._8 .GT. nu) .OR. (0.5_8 .LT. nu)) THEN
     WRITE (0,'("error: -1<=nu<=0.5, nu=",ES9.2E2," given.")') nu
     STOP 1
  END IF

  IF (0 .GT. x3) THEN
     WRITE (0,'("error: observation depth (x3) must be positive")')
     STOP 1
  END IF

  IF ((0 .GT. A(2)) .OR. &
      (0 .GT. B(2)) .OR. &
      (0 .GT. C(2))) THEN
     WRITE (0,'("error: vertex depth must be positive")')
     STOP 1
  END IF

  ! Lame parameter
  lambda=2.d0*nu/(1.d0-2.d0*nu)

  ! isotropic strain
  ekk=e22+e33

  ! unit vectors
  nA = (/ C(2)-B(2), &
          B(1)-C(1) /) / NORM2(C-B)
  nB = (/ C(2)-A(2), &
          A(1)-C(1) /) / NORM2(C-A)
  nC = (/ B(2)-A(2), &
          A(1)-B(1) /) / NORM2(B-A)
  
  ! check that unit vectors are pointing outward
  IF (DOT_PRODUCT(nA,A-(B+C)/2) .GT. 0) THEN
      nA=-nA
  END IF
  IF (DOT_PRODUCT(nB,B-(A+C)/2) .GT. 0) THEN
      nB=-nB
  END IF
  IF (DOT_PRODUCT(nC,C-(A+B)/2) .GT. 0) THEN
      nC=-nC
  END IF

  ! moment density
  m22=lambda*ekk+2.d0*e22
  m23=2.d0*e23
  m33=lambda*ekk+2.d0*e33

  u2= (m22*nC(1)+m23*nC(2))*T22(A,B) &
     +(m23*nC(1)+m33*nC(2))*T32(A,B) &
     +(m22*nA(1)+m23*nA(2))*T22(B,C) &
     +(m23*nA(1)+m33*nA(2))*T32(B,C) &
     +(m22*nB(1)+m23*nB(2))*T22(C,A) &
     +(m23*nB(1)+m33*nB(2))*T32(C,A)

  u3= (m22*nC(1)+m23*nC(2))*T23(A,B) &
     +(m23*nC(1)+m33*nC(2))*T33(A,B) &
     +(m22*nA(1)+m23*nA(2))*T23(B,C) &
     +(m23*nA(1)+m33*nA(2))*T33(B,C) &
     +(m22*nB(1)+m23*nB(2))*T23(C,A) &
     +(m23*nB(1)+m33*nB(2))*T33(C,A)

  ! use double-exponential integration for singular points
  IF (ISNAN(u2+u3)) THEN
     CALL computeDisplacementTriangleStrainVolumePlaneStrainTanhSinh( &
             x2,x3,A,B,C, &
             e22,e23,e33,nu,u2,u3)
  END IF

CONTAINS

  !---------------------------------------------------------------
  !> function IG22
  !! is the integrand of the line integral of G22
  !---------------------------------------------------------------
  REAL*8 FUNCTION IG22(A,B,t)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: A,B
    REAL*8, INTENT(IN) :: t
        
    REAL*8 :: a2,a3,m2,m3,us,ui

    ! azimuthal vector
    a2=(B(1)-A(1))/2
    a3=(B(2)-A(2))/2
    a2=a2/SQRT(a2**2+a3**2)
    a3=a3/SQRT(a2**2+a3**2)

    ! center of segment
    m2=(A(1)+B(1))/2
    m3=(A(2)+B(2))/2
        
    IF (a2 .EQ. 0) THEN
       ! source term
       us=(-0.4d1*(-x3+m3+t)*(nu-0.3d1/0.4d1)*LOG((m3**2+(-2*x3+2*t)*m3+x3**2-2*x3*t+t**2+(-x2+m2)**2)) &
           +0.8d1*((x2-m2)*atan(((-x3+m3+t)/(-x2+m2)))+t)*(nu-0.1d1/0.2d1))/PI/(-0.1d1+nu)/16.d0

       ! first image term
       ui=(nu**2-0.3d1/0.2d1*nu+0.5d1/0.8d1)*((x3+m3+t)*LOG((m3**2+(2*x3+2*t)*m3+x3**2+2*x3*t+t**2+(-x2+m2)**2)) &
           +(2*m2-2*x2)*atan(((x3+m3+t)/(-x2+m2)))-(2*t))/PI/(-0.1d1+nu)/0.2d1

       ! second image term
       ui=ui+(x3*(-x2+m2)*LOG((m3**2+(2*x3+2*t)*m3+x3**2+2*x3*t+t**2+(-x2+m2)**2)) &
           +((-3+4*nu)*m2**2+(6-8*nu)*x2*m2+(-3+4*nu)*x2**2-2*x3**2)*atan(((x3+m3+t)/(-x2+m2))) &
           -0.4d1*(nu-0.3d1/0.4d1)*(-x2+m2)*t)/PI/(-1+nu)/(-x2+m2)/0.8d1

       ! third image term
       ui=ui-x3*((-x2+m2)*(m2**2-2*x2*m2+x2**2+(x3+m3+t)**2)*LOG((m3**2+(2*x3+2*t)*m3+x3**2+2*x3*t+t**2+(-x2+m2)**2)) &
           -x3*(m2**2-2*x2*m2+x2**2+(x3+m3+t)**2)*atan(((x3+m3+t)/(-x2+m2))) &
           +((-x2+m2)*(m2**2-2*x2*m2+x2**2+x3*(x3+m3+t))))/PI/(-x2+m2)/(-1+nu)/(m2**2-2*x2*m2+x2**2+(x3+m3+t)**2)/0.4d1
    ELSE
       ! source term
       us=-((t*(nu-0.3d1/0.4d1)*a2**4/0.2d1+(m2-x2)*(nu-0.3d1/0.4d1)*a2**3/0.2d1 &
           -(-0.2d1*t*(nu-0.3d1/0.4d1)*a3+(-0.5d1/0.4d1+nu)*(x3-m3))*a3*a2**2/0.2d1 &
           +(m2-x2)*(nu-0.1d1/0.4d1)*a3**2*a2/0.2d1-(nu-0.3d1/0.4d1)*a3**3*(x3-m3-a3*t)/0.2d1) &
           *LOG((a2**2+a3**2)*t**2+((0.2d1*m2-0.2d1*x2)*a2+0.2d1*a3*(-x3+m3))*t+x2**2-0.2d1*x2*m2+x3**2+m2**2+m3**2-0.2d1*x3*m3) &
           +((nu-0.1d1/0.2d1)*a3**2+a2**2*(-0.1d1+nu))*((x3-m3)*a2+a3*(m2-x2)) &
           *atan((-a2**2*t+(-m2+x2)*a2-(-x3+m3+a3*t)*a3)/((-x3+m3)*a2-a3*(m2-x2))) &
           -t*(a2**2+a3**2)*((nu-0.3d1/0.4d1)*a2**2+(nu-0.1d1/0.2d1)*a3**2)) &
           /(-0.1d1+nu)/PI/0.2d1

       ! first image term
       ui=((a2**2*t/0.2d1+(-x2/0.2d1+m2/0.2d1)*a2+a3*(x3+m3+a3*t)/0.2d1) &
           *LOG((a2**2+a3**2)*t**2+((0.2d1*m2-0.2d1*x2)*a2+0.2d1*a3*(x3+m3))*t+x2**2-0.2d1*x2*m2+x3**2+m2**2+m3**2+0.2d1*x3*m3) &
           +((x3+m3)*a2-a3*(m2-x2)) &
           *atan((a2**2*t+(m2-x2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(m2-x2))) &
           -(a2**2+a3**2)*t) &
           *(nu**2-0.3d1/0.2d1*nu+0.5d1/0.8d1)/(-0.1d1+nu)/PI
   
       ! second image term
       ui=ui+(-0.1d1/((-m2+x2)*a3+(x3+m3)*a2) &
           *(((-m2+x2)*a3+(x3+m3)*a2) &
           *(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2+2*t*(m2-x2)*a2+x3**2+2*x3*m3+m3**2+(m2-x2)**2)*(a3**3) &
           *LOG(((a2**2+a3**2)*t**2+((2*m2-2*x2)*a2+2*a3*(x3+m3))*t+x2**2-2*x2*m2+x3**2+m2**2+m3**2+2*x3*m3)) &
           +(-a3**4*x3-3*(m2-x2)*a2*a3**3+a2**2*(x3+3*m3)*a3**2-a2**3*(m2-x2)*a3+m3*a2**4) &
           *(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2+2*t*(m2-x2)*a2+x3**2+2*x3*m3+m3**2+(m2-x2)**2) &
           *atan(((a2**2*t+(m2-x2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(m2-x2)))) &
           -((-m2+x2)*a3+(x3+m3)*a2)*(-(t*a3**4*x3)+((-3*t*(m2-x2)*a2-x3**2-x3*m3-(m2-x2)**2)*a3**3) &
           +0.3d1*a2*((t*(x3+m3)*a2)-((m2-x2)*m3)/0.3d1) &
           *(a3**2)+(a2**2*(t*(m2-x2)*a2+x3**2+3*x3*m3+2*m3**2+(m2-x2)**2)*a3)-(m3*a2**3*(a2*t+m2-x2))))*x3 &
           /(-1.d0+nu)/PI/(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2+2*t*(m2-x2)*a2+x3**2+2*x3*m3+m3**2+(m2-x2)**2)/0.4d1)

       ! third image term
       ui=ui+(-0.4d1*((-m2+x2)*a3+(x3+m3)*a2)*(-x3*a3**2/0.4d1 &
           -a2*(m2-x2)*(nu-0.3d1/0.4d1)*a3+a2**2*((x3+m3)*nu-x3-0.3d1/0.4d1*m3))*a3 &
           *LOG((a2**2+a3**2)*t**2+((0.2d1*m2-0.2d1*x2)*a2+0.2d1*a3*(x3+m3))*t+x2**2-0.2d1*x2*m2+x3**2+m2**2+m3**2+0.2d1*x3*m3) &
           +((0.4d1*(m2-x2)**2*nu-0.2d1*x3**2 &
           -0.3d1*(m2-x2)**2)*a3**4-0.8d1*a2*((x3+m3)*nu-x3/0.2d1-0.3d1/0.4d1*m3)*(m2-x2)*a3**3 &
           +0.4d1*a2**2*((x3-m2+m3+x2)*(m2+x3+m3-x2)*nu &
           -0.5d1/0.4d1*x3**2-x3*m3+0.3d1/0.4d1*(m2-m3-x2)*(m2+m3-x2))*a3**2 &
           +0.8d1*a2**3*(m2-x2)*((x3+m3)*nu-x3-0.3d1/0.4d1*m3)*a3 &
           -0.4d1*a2**4*((x3+m3)**2*nu-0.3d1/0.4d1*x3**2-0.2d1*x3*m3-0.3d1/0.4d1*m3**2)) &
           *atan((a2**2*t+(m2-x2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(m2-x2))) &
           -0.4d1*((-m2+x2)*a3+(x3+m3)*a2)*(nu-0.3d1/0.4d1)*a3**2*t) &
           /((-m2+x2)*a3+(x3+m3)*a2)/(-0.1d1+nu)/PI/0.8d1
    END IF

    IG22=us+ui

  END FUNCTION IG22

  !---------------------------------------------------------------
  !> function T22
  !! computes the difference of IG22 between end-points A and B
  !---------------------------------------------------------------
  REAL*8 FUNCTION T22(A,B)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: A, B

    T22=(IG22(A,B,NORM2(B-A)/2)-IG22(A,B,-NORM2(B-A)/2))

  END FUNCTION T22

  !---------------------------------------------------------------
  !> function IG33
  !! is the integrand of the line integral of G33
  !---------------------------------------------------------------
  REAL*8 FUNCTION IG33(A,B,t)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: A,B
    REAL*8, INTENT(IN) :: t
        
    REAL*8 :: a2,a3,m2,m3,us,ui

    ! azimuthal vector
    a2=(B(1)-A(1))/2
    a3=(B(2)-A(2))/2
    a2=a2/SQRT(a2**2+a3**2)
    a3=a3/SQRT(a2**2+a3**2)

    ! center of segment
    m2=(A(1)+B(1))/2
    m3=(A(2)+B(2))/2
        
    IF (0 .EQ. a2) THEN
       ! source term
       us=(-0.4d1*(-x3+m3+t)*(nu-0.3d1/0.4d1)*LOG((m3**2+(-2*x3+2*t)*m3+x3**2-2*x3*t+t**2+(-x2+m2)**2)) &
           -0.8d1*(-0.1d1+nu)*(-x2+m2)*atan(((-x3+m3+t)/(-x2+m2))) &
           -(6.d0*t)+0.8d1*t*nu)/PI/(-0.1d1+nu)/0.16d2

       ! first image term
       ui=(nu**2-0.3d1/0.2d1*nu+0.5d1/0.8d1)*((x3+m3+t)*LOG((m3**2+(2*x3+2*t)*m3+x3**2+2*x3*t+t**2+(-x2+m2)**2)) &
           +(2*m2-2*x2)*atan(((x3+m3+t)/(-x2+m2))) &
           -(2*t))/PI/(-0.1d1+nu)/0.2d1

       ! in the following expression the term x3**2 disappears when
       ! x2=m2 and the solution is finite.
       ui=ui+(-x3*(-x2+m2)*LOG((m3**2+(2*x3+2*t)*m3+x3**2+2*x3*t+t**2+(-x2+m2)**2)) &
           -0.4d1*((nu-0.3d1/0.4d1)*(m2**2) &
                   -0.2d1*(nu-0.3d1/0.4d1)*x2*m2 &
                   +(nu-0.3d1/0.4d1)*(x2**2)-(x3**2)/0.2d1)*atan(((x3+m3+t)/(-x2+m2))) &
             )/PI/(-0.1d1+nu)/(-x2+m2)/0.8d1

       ! third image term
       ui=ui-x3*((x3**2+(2*m3+2*t)*x3+m2**2-2*x2*m2+x2**2+(m3+t)**2)*x3 &
                   *atan(((x3+m3+t)/(-x2+m2))) &
            +((x3**2+(m3+t)*x3+(-x2+m2)**2)*(-x2+m2)))/PI &
            /(x3**2+(2*m3+2*t)*x3+m2**2-2*x2*m2+x2**2+(m3+t)**2)/(-1.d0+nu)/(-x2+m2)/0.4d1
    ELSE
       ! source term
       us=-((t*(nu-0.3d1/0.4d1)*a2**4/0.2d1 &
               +(m2-x2)*(nu-0.3d1/0.4d1)*a2**3/0.2d1 &
               -a3*(-0.2d1*t*(nu-0.3d1/0.4d1)*a3 &
               +(nu-0.1d1/0.4d1)*(x3-m3))*a2**2/0.2d1 &
               +(m2-x2)*(-0.5d1/0.4d1+nu)*a3**2*a2/0.2d1 &
               -(nu-0.3d1/0.4d1)*a3**3*(x3-m3-a3*t)/0.2d1) &
           *LOG((a2**2+a3**2)*t**2+((0.2d1*m2-0.2d1*x2)*a2+0.2d1*a3*(-x3+m3))*t+x2**2-0.2d1*x2*m2+x3**2+m2**2+m3**2-0.2d1*x3*m3) &
           +((-0.1d1+nu)*a3**2+a2**2*(nu-0.1d1/0.2d1))*((x3-m3)*a2+a3*(m2-x2)) &
           *atan((-a2**2*t+(-m2+x2)*a2-(-x3+m3+a3*t)*a3)/((-x3+m3)*a2-a3*(m2-x2))) &
           -t*(a2**2*(nu-0.1d1/0.2d1)+a3**2*(nu-0.3d1/0.4d1))) &
           /PI/(-0.1d1+nu)/0.2d1

       ! first image term
       ui=((nu**2-0.3d1/0.2d1*nu+0.5d1/0.8d1) &
           *((a2**2*t/0.2d1+(m2/0.2d1-x2/0.2d1)*a2+a3*(x3+m3+a3*t)/0.2d1) &
           *LOG(t**2+((0.2d1*m2-0.2d1*x2)*a2+0.2d1*a3*(x3+m3))*t+x2**2-0.2d1*x2*m2+x3**2+m2**2+m3**2+0.2d1*x3*m3)+((x3+m3)*a2-a3*(m2-x2)) &
           *atan((a2**2*t+(m2-x2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(m2-x2))) &
           -(a2**2+a3**2)*t) &
           /PI/(-0.1d1+nu))

       ! second image term
       ui=ui+((0.4d1*a3*((x3+m3)*a2-a3*(-x2+m2))*(((x3+m3)*nu-x3 &
                   -0.3d1/0.4d1*m3)*a2**2 &
                   -(-0.3d1/0.4d1+nu)*a3*(-x2+m2)*a2-a3**2*x3/0.4d1) &
           *LOG((a2**2+a3**2)*t**2+((0.2d1*m2-0.2d1*x2)*a2+0.2d1*a3*(x3+m3))*t+x2**2-0.2d1*x2*m2+x3**2+m2**2+m3**2+0.2d1*x3*m3) &
           +((0.4d1*(x3+m3)**2*nu-0.3d1*m3**2-0.8d1*x3*m3-0.3d1*x3**2)*a2**4 &
               -0.8d1*a3*(-x2+m2)*((x3+m3)*nu-x3-0.3d1/0.4d1*m3)*a2**3 &
               -0.4d1*((m2+x3+m3-x2)*(-m2+x3+m3+x2)*nu &
               -0.5d1/0.4d1*x3**2-x3*m3 &
               +0.3d1/0.4d1*(m2-m3-x2)*(m2+m3-x2))*a3**2*a2**2 &
               +0.8d1*a3**3*((x3+m3)*nu &
               -0.3d1/0.4d1*m3-x3/0.2d1)*(-x2+m2)*a2 &
               +0.2d1*(-0.2d1*(-x2+m2)**2*nu+x3**2 &
               +0.3d1/0.2d1*(-x2+m2)**2)*a3**4) &
           *atan((t*a2**2+(-x2+m2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(-x2+m2))) &
           -0.4d1*(-0.3d1/0.4d1+nu)*t*((x3+m3)*a2-a3*(-x2+m2))*a2**2) &
           /(-0.1d1+nu)/PI/((x3+m3)*a2-a3*(-x2+m2))/0.8d1)

       ! third image term
       ui=ui+(-0.3d1/0.4d1*(-a3*((-m2+x2)*a3+(x3+m3)*a2)*(a2**2) &
           *(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2 &
               +2*t*(-x2+m2)*a2+x3**2+2*x3*m3+m3**2+(-x2+m2)**2) &
           *LOG(((a2**2+a3**2)*t**2+((2*m2-2*x2)*a2+2*a3*(x3+m3))*t+x2**2-2*x2*m2+x3**2+m2**2+m3**2+2*x3*m3))/0.3d1 &
           +(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2+2*t*(-x2+m2)*a2+x3**2+2*x3*m3+m3**2+(-x2+m2)**2) &
           *((a3**4*x3)/0.3d1 &
              -((-x2+m2)*a2*a3**3)/0.3d1 &
              +(x3+m3/0.3d1)*(a2**2)*(a3**2)+(a2**3*(-x2+m2)*a3)/0.3d1 &
              -(m3*a2**4)/0.3d1) &
           *atan(((t*a2**2+(-x2+m2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(-x2+m2)))) &
           -(-(t*a3**4*x3)+((-3*t*(-x2+m2)*a2-x3**2-x3*m3-(-x2+m2)**2)*a3**3) &
               +0.3d1*((t*(x3+m3)*a2)-((-x2+m2)*m3)/0.3d1)*a2*(a3**2) &
               +((t*(-x2+m2)*a2+x3**2+3*x3*m3+2*m3**2+(-x2+m2)**2)*a2**2*a3) &
               -(a2**3*m3*(a2*t+m2-x2)))*((-m2+x2)*a3+(x3+m3)*a2)/0.3d1) &
           *x3/((-m2+x2)*a3+(x3+m3)*a2) &
           /(-1.d0+nu)/PI/(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2+2*t*(-x2+m2)*a2+x3**2+2*x3*m3+m3**2+(-x2+m2)**2))
    END IF
    
    IG33=us+ui

  END FUNCTION IG33


  !---------------------------------------------------------------
  !> function T33
  !! computes the difference of IG33 between end-points A and B
  !---------------------------------------------------------------
  REAL*8 FUNCTION T33(A,B)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: A, B

    T33=(IG33(A,B,NORM2(B-A)/2)-IG33(A,B,-NORM2(B-A)/2))

  END FUNCTION T33


  !---------------------------------------------------------------
  !> function IG23
  !! is the integrand of the line integral of G23
  !---------------------------------------------------------------
  REAL*8 FUNCTION IG23(A,B,t)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: A,B
    REAL*8, INTENT(IN) :: t
        
    REAL*8 :: a2,a3,m2,m3,us,ui

    ! azimuthal vector
    a2=(B(1)-A(1))/2.d0
    a3=(B(2)-A(2))/2.d0
    a2=a2/SQRT(a2**2+a3**2)
    a3=a3/SQRT(a2**2+a3**2)

    ! center of segment
    m2=(A(1)+B(1))/2.d0
    m3=(A(2)+B(2))/2.d0
        
    IF (0 .EQ. a2) THEN
       ! source term
       us=-LOG((m3**2+(-2*x3+2*t)*m3+x3**2-2*x3*t+t**2+(-x2+m2)**2))*(-x2+m2)/PI/(-1.d0+nu)/0.16d2

       ! first image term
       ui=-(nu-0.1d1/0.2d1)*((-m2/0.2d1+x2/0.2d1)*LOG(((m3**2)+((2*x3+2*t)*m3)+(x3**2)+(2*x3*t)+(t**2)+(-x2+m2)**2)/((x3+m3+t)**2)) &
           +(-x3-m3-t)*atan((-x2+m2)/(x3+m3+t)) &
           +LOG((x2-m2)/(x3+m3+t))*(-x2+m2))/PI

       ! second image term
       ui=ui+(LOG((m3**2+(2*x3+2*t)*m3+x3**2+2*x3*t+t**2+(-x2+m2)**2))*(-x2+m2) &
           -0.4d1*atan(((x3+m3+t)/(-x2+m2)))*x3)*(nu-0.3d1/0.4d1)/PI/(-0.1d1+nu)/0.4d1

       ! third image term
       ui=ui-x3*((m3**2+(2*x3+2*t)*m3+x3**2+2*x3*t+t**2+(-x2+m2)**2)*atan(((x3+m3+t)/(-x2+m2))) &
           -((-x2+m2)*(m3+t)))/PI/(-1.d0+nu) &
           /(m3**2+(2*x3+2*t)*m3+x3**2+2*x3*t+t**2+(-x2+m2)**2)/0.4d1
    ELSE
       ! source term
       us=(((x3-m3)*a2+a3*(-x2+m2))*(a2-a3)*(a2+a3) &
           *LOG((t**2+((2.d0*m2-2.d0*x2)*a2+2.d0*a3*(-x3+m3))*t+x2**2-2.d0*x2*m2+x3**2+m2**2+m3**2-2.d0*x3*m3))/0.4d1 &
           +a2*(((x3-m3)*a2+a3*(-x2+m2)) &
           *DATAN(((-t*a2**2+(-m2+x2)*a2-a3*(a3*t-x3+m3))/((-x3+m3)*a2-a3*(-x2+m2)))) &
           -t/0.2d1)*a3)/(-1.d0+nu)/PI/0.4d1

       ! first image term
       IF (0 .NE. a3) THEN
           ui=(-(a3*(-m2+x2)+(x3+m3)*a2)*a3*LOG(((-a2/a3+(a2*x3+a2*m3+a3*x2-a3*m2)/a3/(x3+m3+a3*t))**2+1))/0.2d1 &
               -(a3*(-m2+x2)+(x3+m3)*a2)*a2*atan(((t*a2**2+(-x2+m2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(-x2+m2)))) &
               +(a3*(-m2+x2)+(x3+m3)*a2)*a3*LOG(((a3*(-m2+x2)+(x3+m3)*a2)/(x3+m3+a3*t))) &
               +(a3*(-m2+x2)+(x3+m3)*a2)*a2*atan(a3/a2) &
               +(x3+m3+a3*t)*atan(((-x2+m2+a2*t)/(x3+m3+a3*t))))*(-0.1d1/0.2d1+nu)/PI/a3
       ELSE
           ui=-1.d0/2.d0*((x3+m3)*LOG(((-x2+m2+t)**2+x3**2+2*x3*m3+m3**2)/(x3+m3)**2) &
                   -2.d0*atan((-x2+m2+t)/(x3+m3))*(-x2+m2+t))*(nu-1.d0/2.d0)/PI
       END IF
   
       ! second image term
       ui=ui+((-0.3d1/0.4d1+nu)*(((-x2/0.4d1+m2/0.4d1)*a3**3-0.3d1/0.4d1*(x3+m3/0.3d1)*a2*a3**2 &
                                   -a2**2*(-x2+m2)*a3/0.4d1-a2**3*(x3-m3)/0.4d1) &
           *LOG((a2**2+a3**2)*t**2+((0.2d1*m2-0.2d1*x2)*a2+0.2d1*a3*(x3+m3))*t+x2**2-0.2d1*x2*m2+x3**2+m2**2+m3**2+0.2d1*x3*m3) &
           +((a3**2*x3+(-x2+m2)*a2*a3-a2**2*m3) &
           *atan((t*a2**2+(-x2+m2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(-x2+m2))) &
           +a2*t*(a2**2+a3**2)/0.2d1)*a3)/(-0.1d1+nu)/PI)
   
       ! third image term
       ui=ui+(-0.1d1*x3*(a2*(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2+2*t*(-x2+m2)*a2+m3**2+2*x3*m3+x3**2+(-x2+m2)**2)*(a3**2) &
           *LOG(((a2**2+a3**2)*t**2+((2*m2-2*x2)*a2+2*a3*(x3+m3))*t+x2**2-2*x2*m2+x3**2+m2**2+m3**2+2*x3*m3)) &
           +(a2-a3)*(a2+a3)*(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2+2*t*(-x2+m2)*a2+m3**2+2*x3*m3+x3**2+(-x2+m2)**2)*a3 &
           *atan(((t*a2**2+(-x2+m2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(-x2+m2)))) &
           -(t*(-x2+m2)*a3**4)+(0.3d1*t*(x3+m3/0.3d1)*a2-(m3*(-x2+m2)))*(a3**3) &
           +0.2d1*a2*(0.3d1/0.2d1*t*(-x2+m2)*a2+(m3**2)/0.2d1 &
           +0.3d1/0.2d1*x3*m3+(x3**2)+((-x2+m2)**2))*(a3**2)-(a2**2*(t*(3*m3+x3)*a2+m3*(-x2+m2))*a3)-(m3*a2**3*(x3+m3))) &
           /(-1.d0+nu)/PI/(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2+2*t*(-x2+m2)*a2+m3**2+2*x3*m3+x3**2+(-x2+m2)**2)/0.4d1)
       
    END IF

    IG23=us+ui

  END FUNCTION IG23

  !---------------------------------------------------------------
  !> function T23
  !! computes the difference of IG23 between end-points A and B
  !---------------------------------------------------------------
  REAL*8 FUNCTION T23(A,B)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: A, B

    T23=(IG23(A,B,NORM2(B-A)/2)-IG23(A,B,-NORM2(B-A)/2))

  END FUNCTION T23


  !---------------------------------------------------------------
  !> function IG32
  !! is the integrand of the line integral of G32
  !---------------------------------------------------------------
  REAL*8 FUNCTION IG32(A,B,t)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: A,B
    REAL*8, INTENT(IN) :: t
        
    REAL*8 :: a2,a3,m2,m3,us,ui

    ! azimuthal vector
    a2=(B(1)-A(1))/2.d0
    a3=(B(2)-A(2))/2.d0
    a2=a2/SQRT(a2**2+a3**2)
    a3=a3/SQRT(a2**2+a3**2)

    ! center of segment
    m2=(A(1)+B(1))/2.d0
    m3=(A(2)+B(2))/2.d0
        
    IF (0 .EQ. a2) THEN
       ! source term
       us=-LOG((m3**2+(-2.d0*x3+2*t)*m3+x3**2-2.d0*x3*t+t**2+(-x2+m2)**2))*(-x2+m2)/PI/(-1.d0+nu)/0.16d2

       ! first image term
       ui=(nu-0.1d1/0.2d1)*((-m2/0.2d1+x2/0.2d1)*LOG(((m3**2)+((2*x3+2*t)*m3)+(x3**2)+(2*x3*t)+(t**2)+(-x2+m2)**2)/((x3+m3+t)**2)) &
           +(-x3-m3-t)*atan((-x2+m2)/(x3+m3+t)) &
           +LOG((x2-m2)/(x3+m3+t))*(-x2+m2))/PI

       ! second image term
       ui=ui+(LOG((m3**2+(2*x3+2*t)*m3+x3**2+2*x3*t+t**2+(-x2+m2)**2))*(-x2+m2) &
           -0.4d1*atan(((x3+m3+t)/(-x2+m2)))*x3)*(nu-0.3d1/0.4d1)/PI/(-0.1d1+nu)/0.4d1

       ! third image term
       ui=ui+x3*((m3**2+(2*x3+2*t)*m3+x3**2+2*x3*t+t**2+(-x2+m2)**2)*atan(((x3+m3+t)/(-x2+m2)))-((-x2+m2)*(m3+t)))/PI/ &
           (m3**2+(2*x3+2*t)*m3+x3**2+2*x3*t+t**2+(-x2+m2)**2)/(-1.d0+nu)/0.4d1
    ELSE
    ! source term
    us=(((x3-m3)*a2+a3*(-x2+m2))*(a2-a3)*(a2+a3) &
        *LOG(((a2**2+a3**2)*t**2+((2*m2-2*x2)*a2+2*a3*(-x3+m3))*t+x2**2-2*x2*m2+x3**2+m2**2+m3**2-2*x3*m3))/0.4d1 &
        +a2*(((x3-m3)*a2+a3*(-x2+m2))*atan(((-t*a2**2+(-m2+x2)*a2-a3*(a3*t-x3+m3))/((-x3+m3)*a2-a3*(-x2+m2)))) &
        -((a2**2+a3**2)*t)/0.2d1)*a3)/(-1.d0+nu)/PI/0.4d1

    ! first image term
    IF (0 .NE. a3) THEN
       ui=-(-(a3*(-m2+x2)+(x3+m3)*a2)*a3*LOG(((-a2/a3+(a2*x3+a2*m3+a3*x2-a3*m2)/a3/(x3+m3+a3*t))**2+1))/0.2d1 &
            -(a3*(-m2+x2)+(x3+m3)*a2)*a2*atan(((t*a2**2+(-x2+m2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(-x2+m2)))) &
            +(a3*(-m2+x2)+(x3+m3)*a2)*a3*LOG(((a3*(-m2+x2)+(x3+m3)*a2)/(x3+m3+a3*t))) &
            +(a3*(-m2+x2)+(x3+m3)*a2)*a2*atan(a3/a2) &
            +(x3+m3+a3*t)*atan(((-x2+m2+a2*t)/(x3+m3+a3*t))))*(-0.1d1/0.2d1+nu)/PI/a3
    ELSE
       ui=1.d0/2*((x3+m3)*LOG(((-x2+m2+t)**2+x3**2+2*x3*m3+m3**2)/(x3+m3)**2) &
               -2*atan((-x2+m2+t)/(x3+m3))*(-x2+m2+t))*(nu-1.d0/2.d0)/PI
    END IF

    ! second image term
    ui=ui+((-0.3d1/0.4d1+nu)*(((-x2/0.4d1+m2/0.4d1)*a3**3-0.3d1/0.4d1*(x3+m3/0.3d1)*a2*a3**2-a2**2*(-x2+m2)*a3/0.4d1 &
          -a2**3*(x3-m3)/0.4d1)*LOG((a2**2+a3**2)*t**2+((0.2d1*m2-0.2d1*x2)*a2+0.2d1*a3*(x3+m3))*t+x2**2-0.2d1*x2*m2+x3**2+m2**2+m3**2+0.2d1*x3*m3) &
          +((a3**2*x3+(-x2+m2)*a2*a3-a2**2*m3) &
          *atan((t*a2**2+(-x2+m2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(-x2+m2)))+a2*t*(a2**2+a3**2)/0.2d1)*a3)/(-0.1d1+nu)/PI)

    ! third image term
    ui=ui-(-0.1e1*x3*(a2*(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2+2*t*(-x2+m2)*a2+m3**2+2*x3*m3+x3**2+(-x2+m2)**2)*(a3**2) &
           *LOG(((a2**2+a3**2)*t**2+((2*m2-2*x2)*a2+2*a3*(x3+m3))*t+x2**2-2*x2*m2+x3**2+m2**2+m3**2+2*x3*m3)) &
           +(a2-a3)*(a2+a3)*(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2+2*t*(-x2+m2)*a2+m3**2+2*x3*m3+x3**2+(-x2+m2)**2)*a3 &
           *atan(((t*a2**2+(-x2+m2)*a2+a3*(x3+m3+a3*t))/((x3+m3)*a2-a3*(-x2+m2)))) &
           -(t*(-x2+m2)*a3**4) &
           +(0.3e1*t*(x3+m3/0.3e1)*a2-(m3*(-x2+m2)))*(a3**3) &
           +0.2e1*a2*(0.3e1/0.2e1*t*(-x2+m2)*a2+(m3**2)/0.2e1 &
           +0.3e1/0.2e1*x3*m3+(x3**2)+((-x2+m2)**2))*(a3**2)-(a2**2*(t*(3*m3+x3)*a2+m3*(-x2+m2))*a3)-(m3*a2**3*(x3+m3))) &
          /(-1+nu)/pi &
          /(a3**2*t**2+2*t*(x3+m3)*a3+a2**2*t**2+2*t*(-x2+m2)*a2+m3**2+2*x3*m3+x3**2+(-x2+m2)**2)/0.4e1)
        
    END IF

    IG32=us+ui

  END FUNCTION IG32

  !---------------------------------------------------------------
  !> function T32
  !! computes the difference of IG32 between end-points A and B
  !---------------------------------------------------------------
  REAL*8 FUNCTION T32(A,B)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: A, B

    T32=(IG32(A,B,NORM2(B-A)/2)-IG32(A,B,-NORM2(B-A)/2))

  END FUNCTION T32

END SUBROUTINE computeDisplacementTriangleStrainVolumePlaneStrain



