function [u22,u23,u32,u33]=computeGradientPlaneStrainTriangleFiniteDifference( ...
    x2,x3,A,B,C,e22,e23,e33,nu)
% function COMPUTEGRADIENTPLANESTRAINTRIANGLEFINITEDIFFERENCE computes the
% displacement gradient associated with deforming triangle volume element
% considering the following geometry using the analytic solution of
% Barbot (2018).
%
%              surface
%      -------------+-------------- E (x2)
%                   |
%                   |     + A
%                   |    /  . 
%                   |   /     .  
%                   |  /        .            
%                   | /           .      
%                   |/              + B
%                   /            .
%                  /|          /  
%                 / :       .
%                /  |    /
%               /   : .
%              /   /|
%             / .   :
%            +      |
%          C        :
%                   |
%                   D (x3)
%
%
% Input:
% x2, x3             east coordinates and depth of the observation point,
% A, B, C            east and depth coordinates of the vertices,
% eij                source strain component 22, 23 and 33 in the 
%                    volume element,
% nu                 Poisson's ratio in the half space.
%
% Output:
% uij                derivative in the j direction of displacement component ui.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - March 7, 2018, Singapore.

assert(min(x3(:))>=0,'depth must be positive.');

% Lame parameter
lambda=2*nu/(1-2*nu);

% unit vectors
nA = [C(2)-B(2);
      B(1)-C(1)]/norm(C-B);
nB = [C(2)-A(2);
      A(1)-C(1)]/norm(C-A);
nC = [B(2)-A(2);
      A(1)-B(1)]/norm(B-A);
  
% check that unit vectors are pointing outward
if (nA'*(A(:)-(B(:)+C(:))/2))>0
    nA=-nA;
end
if (nB'*(B(:)-(A(:)+C(:))/2))>0
    nB=-nB;
end
if (nC'*(C(:)-(A(:)+B(:))/2))>0
    nC=-nC;
end

l=min([sqrt((A(1)-B(1))^2+(A(2)-B(2))^2), ...
       sqrt((A(1)-C(1))^2+(A(2)-C(2))^2), ...
       sqrt((B(1)-C(1))^2+(B(2)-C(2))^2), ...
    ]);

step=1e-6;

p2=l*step;
p3=l*step;

[u2p2,u3p2]=computeDisplacementPlaneStrainTriangle( ...
    x2+p2,x3,A,B,C,e22,e23,e33,nu);
[u2m2,u3m2]=computeDisplacementPlaneStrainTriangle( ...
    x2-p2,x3,A,B,C,e22,e23,e33,nu);

[u2p3,u3p3]=computeDisplacementPlaneStrainTriangle( ...
    x2,x3+p3,A,B,C,e22,e23,e33,nu);
[u2m3,u3m3]=computeDisplacementPlaneStrainTriangle( ...
    x2,x3   ,A,B,C,e22,e23,e33,nu);

% displacement gradient components
u22=(u2p2-u2m2)/(2*l*step);
u23=(u2p3-u2m3)/(l*step);
u32=(u3p2-u3m2)/(2*l*step);
u33=(u3p3-u3m3)/(l*step);

end
