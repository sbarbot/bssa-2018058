function [s12,s13]=computeStressAntiplaneTriangleMixedQuad( ...
    x2,x3,A,B,C,e12,e13,G)
% function COMPUTESTRESSANTIPLANETRIANGLEMIXEDQUAD computes
% the stress field associated with deforming triangle volume element
% considering the following geometry using the double-exponential or the 
% Gauss quadrature based on distance from the circumcenter.
%
%              surface
%      -------------+-------------- E (x2)
%                   |
%                   |     + A
%                   |    /  .
%                   |   /     .
%                   |  /        .
%                   | /           .
%                   |/              + B
%                   /            .
%                  /|          /  
%                 / :       .
%                /  |    /
%               /   : .
%              /   /|
%             / .   :
%            +      |
%          C        :
%                   |
%                   D (x3)
%
% Input:
% x2, x3             east coordinates and depth of the observation point,
% A, B, C            east and depth coordinates of the vertices,
% e12, e13           source strain component 12 and 13 in the volume element.
% G                  shear modulus in the half-space.
%
% Output:
% s12, s13           horizontal and vertical shear stress component.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - April 7, 2018, Los Angeles.

assert(min(x3(:))>=0,'depth must be positive.');

% unit vectors
nA = [C(2)-B(2);
      B(1)-C(1)]/norm(C-B);
nB = [C(2)-A(2);
      A(1)-C(1)]/norm(C-A);
  
% check that unit vectors are pointing outward
if (nA'*(A(:)-(B(:)+C(:))/2))>0
    nA=-nA;
end
if (nB'*(B(:)-(A(:)+C(:))/2))>0
    nB=-nB;
end

% circumcenter of triangle
O=((B(:)+C(:))+nA*(nB(2)*(B(1)-A(1))-nB(1)*(B(2)-A(2)))/(nA(2)*nB(1)-nA(1)*nB(2)))/2;

% circumcircle radius
r=norm(O(:)-A(:));

inside=sqrt((x2-O(1)).^2+(x3-O(2)).^2)<1.75*r;
outside=~inside;

% initiate empty array
s12=zeros(size(x2));
s13=zeros(size(x2));

% numerical solution with Gauss quadrature for points outside the circumcircle
if numel(x3(outside))>0
    [s12(outside),s13(outside)]=computeStressAntiplaneTriangleGauss( ...
        x2(outside),x3(outside),A,B,C,e12,e13,G);
end

% numerical solution with double-exponential quadrature for points inside the circumcircle
if numel(x3(inside))>0
    [s12(inside),s13(inside)]=computeStressAntiplaneTriangleTanhSinh( ...
        x2(inside),x3(inside),A,B,C,e12,e13,G);
end

end












