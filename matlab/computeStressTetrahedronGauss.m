function [s11,s12,s13,s22,s23,s33]=computeStressTetrahedronGauss( ...
    x1,x2,x3,A,B,C,D,e11,e12,e13,e22,e23,e33,G,nu,varargin)
% function COMPUTESTRESSTETRAHEDRONGAUSS computes the
% stress field associated with deforming tetrahedral volume element
% considering the following geometry using the Gauss-Legendre numerical
% quadrature.
%
%                      / North (x1)
%                     /
%        surface     /
%      -------------+-------------- East (x2)
%                  /|
%                 / |     + A
%                /  |    /  .
%                   |   /     .
%                   |  /        .
%                   | /           .
%                   |/              + B
%                   /            .  |
%                  /|          /    |
%                 / :       .       |
%                /  |    /          |
%               /   : .             |
%              /   /|               |
%             / .   :               |
%            +------|---------------+
%          C        :                 D
%                   |
%                   Down (x3)
%
%
% Input:
% x1, x2, x3         north, east coordinates and depth of the observation point,
% A, B, C, D         north, east, and depth coordinates of the vertices,
% eij                source strain component 11, 12, 13, 22, 23 and 33
%                    in the volume element,
% G, nu              rigidity and Poisson's ratio in the half space.
%
% Option:
% 'N',integer        number of integration points [15]
%
% Output:
% sij                stress components ij.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - May 19, 2018, Los Angeles.
%                                          - May 12, 2022, Tokyo.

assert(min(x3(:))>=0,'depth must be positive.');

% process optional input
p = inputParser;
p.addParameter('N',15,@validateN);
p.parse(varargin{:});
optionStruct = p.Results;

% Lame parameter
lambda=2*nu/(1-2*nu);

% isotropic strain
ekk=e11+e22+e33;

% unit normal vectors
nA=cross(C-B,D-B); % BCD
nB=cross(D-C,A-C); % CDA
nC=cross(A-D,B-D); % DAB
nD=cross(B-A,C-A); % ABC

nA=nA/norm(nA);
nB=nB/norm(nB);
nC=nC/norm(nC);
nD=nD/norm(nD);

% check that unit vectors are pointing outward
if (nA'*(A(:)-(B(:)+C(:)+D(:))/3))>0
    nA=-nA;
end
if (nB'*(B(:)-(C(:)+D(:)+A(:))/3))>0
    nB=-nB;
end
if (nC'*(C(:)-(D(:)+A(:)+B(:))/3))>0
    nC=-nC;
end
if (nD'*(D(:)-(A(:)+B(:)+C(:))/3))>0
    nD=-nD;
end

% area of triangle ABC
ABC=norm(cross(C-A,B-A))/2;
% area of triangle BCD
BCD=norm(cross(D-B,C-B))/2;
% area of triangle CDA
CDA=norm(cross(A-C,D-C))/2;
% area of triangle DAB
DAB=norm(cross(B-D,A-D))/2;

% Radii
r1=@(y1,y2,y3) sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
r2=@(y1,y2,y3) sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);

%% Green's functions
G11d1=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*( ...
    -(3-4*nu)./r1(y1,y2,y3).^3 ...
    -1./r2(y1,y2,y3).^3 ...
    +(2*r1(y1,y2,y3).^2-3*(x1-y1).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(2*r2(y1,y2,y3).^2-3*(x1-y1).^2)./r2(y1,y2,y3).^5 ...
    -6*y3*x3.*(3*r2(y1,y2,y3).^2-5*(x1-y1).^2)./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3).^2) ...
    -8*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3).^2) ...
        +4*(1-2*nu)*(1-nu)*(x1-y1).^2./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
        +8*(1-2*nu)*(1-nu)*(x1-y1).^2./(r2(y1,y2,y3).^2.*(r2(y1,y2,y3)+x3+y3).^3) ...
    );

G11d2=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x2-y2).*( ...
    -(3-4*nu)./r1(y1,y2,y3).^3 ...
    -1./r2(y1,y2,y3).^3 ...
    -3*(x1-y1).^2./r1(y1,y2,y3).^5 ...
    -3*(3-4*nu)*(x1-y1).^2./r2(y1,y2,y3).^5 ...
    -6*y3*x3.*(r2(y1,y2,y3).^2-5*(x1-y1).^2)./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3).^2) ...
    +4*(1-2*nu)*(1-nu)*(x1-y1).^2.*(3*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^3) ...
    );

G11d3=@(y1,y2,y3) 1/(16*pi*(1-nu))*( ...
    -(3-4*nu)*(x3-y3)./r1(y1,y2,y3).^3 ...
    -(x3+y3)./r2(y1,y2,y3).^3 ...
    -3*(x1-y1).^2.*(x3-y3)./r1(y1,y2,y3).^5 ...
    -3*(3-4*nu)*(x1-y1).^2.*(x3+y3)./r2(y1,y2,y3).^5 ...
    +2*y3*(r2(y1,y2,y3).^2-3*x3.*(x3+y3))./r2(y1,y2,y3).^5 ... 
    -6*y3*(x1-y1).^2.*(r2(y1,y2,y3).^2-5*x3.*(x3+y3))./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3)) ...
    +4*(1-2*nu)*(1-nu)*(x1-y1).^2.*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G21d1=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x2-y2).*( ...
    +(r1(y1,y2,y3).^2-3*(x1-y1).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(r2(y1,y2,y3).^2-3*(x1-y1).^2)./r2(y1,y2,y3).^5 ...
    -6*y3*x3.*(r2(y1,y2,y3).^2-5*(x1-y1).^2)./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3).^2) ...
    +4*(1-2*nu)*(1-nu)*(x1-y1).^2.*(3*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^3) ...
    );

G21d2=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*( ...
    +(r1(y1,y2,y3).^2-3*(x2-y2).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(r2(y1,y2,y3).^2-3*(x2-y2).^2)./r2(y1,y2,y3).^5 ...
    -6*y3*x3.*(r2(y1,y2,y3).^2-5*(x2-y2).^2)./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3).^2) ...
    +4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(3*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^3) ...
    );

G21d3=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
    -3*(x3-y3)./r1(y1,y2,y3).^5 ...
    -3*(3-4*nu)*(x3+y3)./r2(y1,y2,y3).^5 ...
    -6*y3*(r2(y1,y2,y3).^2-5*x3.*(x3+y3))./r2(y1,y2,y3).^7 ...
    +4*(1-2*nu)*(1-nu)*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G31d1=@(y1,y2,y3) 1/(16*pi*(1-nu))*( ...
    +(x3-y3).*(r1(y1,y2,y3).^2-3*(x1-y1).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(x3-y3).*(r2(y1,y2,y3).^2-3*(x1-y1).^2)./r2(y1,y2,y3).^5 ...
    +6*y3*x3.*(x3+y3).*(r2(y1,y2,y3).^2-5*(x1-y1).^2)./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3)) ...
    +4*(1-2*nu)*(1-nu)*(x1-y1).^2.*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G31d2=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
    -3*(x3-y3)./r1(y1,y2,y3).^5 ...
    -3*(3-4*nu)*(x3-y3)./r2(y1,y2,y3).^5 ...
    -30*y3*x3.*(x3+y3)./r2(y1,y2,y3).^7 ...
    +4*(1-2*nu)*(1-nu)*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G31d3=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*( ...
    +(r1(y1,y2,y3).^2-3*(x3-y3).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(r2(y1,y2,y3).^2-3*(x3.^2-y3.^2))./r2(y1,y2,y3).^5 ...
    +6*y3*(2*x3+y3)./r2(y1,y2,y3).^5 ...
    -30*y3*x3.*(x3+y3).^2./r2(y1,y2,y3).^7 ...
    +4*(1-2*nu)*(1-nu)./r2(y1,y2,y3).^3 ...
    );

G12d1=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x2-y2).*( ...
    +(r1(y1,y2,y3).^2-3*(x1-y1).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(r2(y1,y2,y3).^2-3*(x1-y1).^2)./r2(y1,y2,y3).^5 ...
    -6*y3*x3.*(r2(y1,y2,y3).^2-5*(x1-y1).^2)./r2(y1,y2,y3).^7 ...
    -4*(1-nu)*(1-2*nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3).^2) ...
    +4*(1-nu)*(1-2*nu)*(x1-y1).^2.*(3*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^3) ...
    );

G12d2=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*( ...
    +(r1(y1,y2,y3).^2-3*(x2-y2).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(r2(y1,y2,y3).^2-3*(x2-y2).^2)./r2(y1,y2,y3).^5 ...
    -6*y3*x3.*(r2(y1,y2,y3).^2-5*(x2-y2).^2)./r2(y1,y2,y3).^7 ...
    -4*(1-nu)*(1-2*nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3).^2) ...
    +4*(1-nu)*(1-2*nu)*(x2-y2).^2.*(3*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^3) ...
	);

G12d3=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
    -3*(x3-y3)./r1(y1,y2,y3).^5 ...
    -3*(3-4*nu)*(x3+y3)./r2(y1,y2,y3).^5 ...
    -6*y3*(r2(y1,y2,y3).^2-5*x3.*(x3+y3))./r2(y1,y2,y3).^7 ...
    +4*(1-2*nu)*(1-nu)*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G22d1=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*( ...
    -(3-4*nu)./r1(y1,y2,y3).^3 ...
    -1./r2(y1,y2,y3).^3 ...
    -3*(x2-y2).^2./r1(y1,y2,y3).^5 ...
    -3*(3-4*nu)*(x2-y2).^2./r2(y1,y2,y3).^5 ...
    -6*y3*x3.*(r2(y1,y2,y3).^2-5*(x2-y2).^2)./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3).^2) ...
    +4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(3*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^3) ...
    );

G22d2=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x2-y2).*( ...
    -(3-4*nu)./r1(y1,y2,y3).^3 ...
    -1./r2(y1,y2,y3).^3 ...
    +(2*r1(y1,y2,y3).^2-3*(x2-y2).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(2*r2(y1,y2,y3).^2-3*(x2-y2).^2)./r2(y1,y2,y3).^5 ...
    -6*y3*x3.*(3*r2(y1,y2,y3).^2-5*(x2-y2).^2)./r2(y1,y2,y3).^7 ...
    -12*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3).^2) ...
    +4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(3*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^3) ...
    );

G22d3=@(y1,y2,y3) 1/(16*pi*(1-nu))*( ...
    -(3-4*nu)*(x3-y3)./r1(y1,y2,y3).^3 ...
    -(x3+y3)./r2(y1,y2,y3).^3 ...
    -3*(x2-y2).^2.*(x3-y3)./r1(y1,y2,y3).^5 ...
    -3*(3-4*nu)*(x2-y2).^2.*(x3+y3)./r2(y1,y2,y3).^5 ...
    +2*y3*(r2(y1,y2,y3).^2-3*x3.*(x3+y3))./r2(y1,y2,y3).^5 ... 
    -6*y3*(x2-y2).^2.*(r2(y1,y2,y3).^2-5*x3.*(x3+y3))./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3)) ...
    +4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G32d1=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
    -3*(x3-y3)./r1(y1,y2,y3).^5 ...
    -3*(3-4*nu)*(x3-y3)./r2(y1,y2,y3).^5 ...
    -30*y3*x3.*(x3+y3)./r2(y1,y2,y3).^7 ...
    +4*(1-2*nu)*(1-nu)*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G32d2=@(y1,y2,y3) 1/(16*pi*(1-nu))*( ...
    +(x3-y3).*(r1(y1,y2,y3).^2-3*(x2-y2).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(x3-y3).*(r2(y1,y2,y3).^2-3*(x2-y2).^2)./r2(y1,y2,y3).^5 ...
    +6*y3*x3.*(x3+y3).*(r2(y1,y2,y3).^2-5*(x2-y2).^2)./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3)) ...
    +4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G32d3=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x2-y2).*( ...
    +(r1(y1,y2,y3).^2-3*(x3-y3).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(r2(y1,y2,y3).^2-3*(x3.^2-y3^2))./r2(y1,y2,y3).^5 ...
    +6*y3*(2*x3+y3)./r2(y1,y2,y3).^5 ...
    -30*y3*x3.*(x3+y3).^2./r2(y1,y2,y3).^7 ...
    +4*(1-2*nu)*(1-nu)./r2(y1,y2,y3).^3 ...
    );

G13d1=@(y1,y2,y3) 1/(16*pi*(1-nu))*( ...
    +(x3-y3).*(r1(y1,y2,y3).^2-3*(x1-y1).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(x3-y3).*(r2(y1,y2,y3).^2-3*(x1-y1).^2)./r2(y1,y2,y3).^5 ...
    -6*y3*x3.*(x3+y3).*(r2(y1,y2,y3).^2-5*(x1-y1).^2)./r2(y1,y2,y3).^7 ...
    +4*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3)) ...
    -4*(1-2*nu)*(1-nu)*(x1-y1).^2.*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G13d2=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
    -3*(x3-y3)./r1(y1,y2,y3).^5 ...
    -3*(3-4*nu)*(x3-y3)./r2(y1,y2,y3).^5 ...
    +30*y3*x3.*(x3+y3)./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G13d3=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*( ...
    +(r1(y1,y2,y3).^2-3*(x3-y3).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(r2(y1,y2,y3).^2-3*(x3.^2-y3^2))./r2(y1,y2,y3).^5 ...
    -6*y3*(2*x3+y3)./r2(y1,y2,y3).^5 ...
    +30*y3*x3.*(x3+y3).^2./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)./r2(y1,y2,y3).^3 ...
    );

G23d1=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
    -3*(x3-y3)./r1(y1,y2,y3).^5 ...
    -3*(3-4*nu)*(x3-y3)./r2(y1,y2,y3).^5 ...
    +30*y3*x3.*(x3+y3)./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G23d2=@(y1,y2,y3) 1/(16*pi*(1-nu))*( ...
    +(x3-y3).*(r1(y1,y2,y3).^2-3*(x2-y2).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(x3-y3).*(r2(y1,y2,y3).^2-3*(x2-y2).^2)./r2(y1,y2,y3).^5 ...
    -6*y3*x3.*(x3+y3).*(r2(y1,y2,y3).^2-5*(x2-y2).^2)./r2(y1,y2,y3).^7 ...
    +4*(1-2*nu)*(1-nu)./(r2(y1,y2,y3).*(r2(y1,y2,y3)+x3+y3)) ...
    -4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(2*r2(y1,y2,y3)+x3+y3)./(r2(y1,y2,y3).^3.*(r2(y1,y2,y3)+x3+y3).^2) ...
    );

G23d3=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x2-y2).*( ...
    +(r1(y1,y2,y3).^2-3*(x3-y3).^2)./r1(y1,y2,y3).^5 ...
    +(3-4*nu)*(r2(y1,y2,y3).^2-3*(x3.^2-y3^2))./r2(y1,y2,y3).^5 ...
    -6*y3*(2*x3+y3)./r2(y1,y2,y3).^5 ...
    +30*y3*x3.*(x3+y3).^2./r2(y1,y2,y3).^7 ...
    -4*(1-2*nu)*(1-nu)./r2(y1,y2,y3).^3 ...
    );

G33d1=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x1-y1).*( ...
    -(3-4*nu)./r1(y1,y2,y3).^3 ...
    -(5-12*nu+8*nu^2)./r2(y1,y2,y3).^3 ...
    -3*(x3-y3).^2./r1(y1,y2,y3).^5 ...
    -30*y3*x3.*(x3+y3).^2./r2(y1,y2,y3).^7 ...
    -3*(3-4*nu)*(x3+y3).^2./r2(y1,y2,y3).^5 ...
    +6*y3*x3./r2(y1,y2,y3).^5 ...
    );

G33d2=@(y1,y2,y3) 1/(16*pi*(1-nu))*(x2-y2).*( ...
    -(3-4*nu)./r1(y1,y2,y3).^3 ...
    -(5-12*nu+8*nu^2)./r2(y1,y2,y3).^3 ...
    -3*(x3-y3).^2./r1(y1,y2,y3).^5 ...
    -30*y3*x3.*(x3+y3).^2./r2(y1,y2,y3).^7 ...
    -3*(3-4*nu)*(x3+y3).^2./r2(y1,y2,y3).^5 ...
    +6*y3*x3./r2(y1,y2,y3).^5 ...
    );

G33d3=@(y1,y2,y3) 1/(16*pi*(1-nu))*( ...
    -(3-4*nu)*(x3-y3)./r1(y1,y2,y3).^3 ...
    -(5-12*nu+8*nu^2)*(x3+y3)./r2(y1,y2,y3).^3 ...
    +(x3-y3).*(2*r1(y1,y2,y3).^2-3*(x3-y3).^2)./r1(y1,y2,y3).^5 ...
    +6*y3*(x3+y3).^2./r2(y1,y2,y3).^5 ...
    +6*y3*x3.*(x3+y3).*(2*r2(y1,y2,y3).^2-5*(x3+y3).^2)./r2(y1,y2,y3).^7 ...
    +(3-4*nu)*(x3+y3).*(2*r2(y1,y2,y3).^2-3*(x3+y3).^2)./r2(y1,y2,y3).^5 ...
    -2*y3*(r2(y1,y2,y3).^2-3*x3.*(x3+y3))./r2(y1,y2,y3).^5 ...
    );

% parameterized surface integral
y=@(u,v,A,B,C) A*(1-u)*(1-v)/4+B*(1+u)*(1-v)/4+C*(1+v)/2;

% moment density
m11=lambda*ekk+2*e11;
m12=2*e12;
m13=2*e13;
m22=lambda*ekk+2*e22;
m23=2*e23;
m33=lambda*ekk+2*e33;

%% numerical solution with Gauss-Legendre quadrature
[x,w]=gaussxw(-1,1,optionStruct.N);

u11=zeros(size(x1));
u12=zeros(size(x1));
u13=zeros(size(x1));

u21=zeros(size(x1));
u22=zeros(size(x1));
u23=zeros(size(x1));

u31=zeros(size(x1));
u32=zeros(size(x1));
u33=zeros(size(x1));

for k=1:length(x)
    for j=1:length(x)
        u11=u11+w(j)*w(k)*(1-x(k))*IU11(x(j),x(k));
        u12=u12+w(j)*w(k)*(1-x(k))*IU12(x(j),x(k));
        u13=u13+w(j)*w(k)*(1-x(k))*IU13(x(j),x(k));
        
        u21=u21+w(j)*w(k)*(1-x(k))*IU21(x(j),x(k));
        u22=u22+w(j)*w(k)*(1-x(k))*IU22(x(j),x(k));
        u23=u23+w(j)*w(k)*(1-x(k))*IU23(x(j),x(k));
        
        u31=u31+w(j)*w(k)*(1-x(k))*IU31(x(j),x(k));
        u32=u32+w(j)*w(k)*(1-x(k))*IU32(x(j),x(k));
        u33=u33+w(j)*w(k)*(1-x(k))*IU33(x(j),x(k));
    end
end

% remove anelastic strain
omega=  heaviside(((A(1)+B(1)+C(1))/3-x1)*nD(1)+((A(2)+B(2)+C(2))/3-x2)*nD(2)+((A(3)+B(3)+C(3))/3-x3)*nD(3)) ...
      .*heaviside(((B(1)+C(1)+D(1))/3-x1)*nA(1)+((B(2)+C(2)+D(2))/3-x2)*nA(2)+((B(3)+C(3)+D(3))/3-x3)*nA(3)) ...
      .*heaviside(((C(1)+D(1)+A(1))/3-x1)*nB(1)+((C(2)+D(2)+A(2))/3-x2)*nB(2)+((C(3)+D(3)+A(3))/3-x3)*nB(3)) ...
      .*heaviside(((D(1)+A(1)+B(1))/3-x1)*nC(1)+((D(2)+A(2)+B(2))/3-x2)*nC(2)+((D(3)+A(3)+B(3))/3-x3)*nC(3));

% strain components
e11=u11        -omega*e11;
e12=(u12+u21)/2-omega*e12;
e13=(u13+u31)/2-omega*e13;
e22=u22        -omega*e22;
e23=(u23+u32)/2-omega*e23;
e33=u33        -omega*e33;

% divergence
div=e11+e22+e33;

s11=2*G*e11+G*lambda*div;
s12=2*G*e12;
s13=2*G*e13;
s22=2*G*e22+G*lambda*div;
s23=2*G*e23;
s33=2*G*e33+G*lambda*div;

    function d = IU11(u,v)
        % function IU11 is the integrand for displacement gradient u1,1
        d=zeros(size(x1));
        if (m11*nD(1)+m12*nD(2)+m13*nD(3)) ~= 0
            d=d+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G11d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m12*nD(1)+m22*nD(2)+m23*nD(3)) ~= 0
            d=d+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G21d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m13*nD(1)+m23*nD(2)+m33*nD(3)) ~= 0
            d=d+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G31d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m11*nA(1)+m12*nA(2)+m13*nA(3)) ~= 0
            d=d+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G11d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m12*nA(1)+m22*nA(2)+m23*nA(3)) ~= 0
            d=d+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G21d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m13*nA(1)+m23*nA(2)+m33*nA(3)) ~= 0
            d=d+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G31d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m11*nB(1)+m12*nB(2)+m13*nB(3)) ~= 0
            d=d+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G11d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m12*nB(1)+m22*nB(2)+m23*nB(3)) ~= 0
            d=d+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G21d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m13*nB(1)+m23*nB(2)+m33*nB(3)) ~= 0
            d=d+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G31d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m11*nC(1)+m12*nC(2)+m13*nC(3)) ~= 0
            d=d+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G11d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m12*nC(1)+m22*nC(2)+m23*nC(3)) ~= 0
            d=d+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G21d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m13*nC(1)+m23*nC(2)+m33*nC(3)) ~= 0
            d=d+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G31d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
    end

    function d = IU12(u,v)
        % function IU12 is the integrand for displacement gradient u1,2
        d=zeros(size(x1));
        if (m11*nD(1)+m12*nD(2)+m13*nD(3)) ~= 0
            d=d+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G11d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m12*nD(1)+m22*nD(2)+m23*nD(3)) ~= 0
            d=d+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G21d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m13*nD(1)+m23*nD(2)+m33*nD(3)) ~= 0
            d=d+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G31d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m11*nA(1)+m12*nA(2)+m13*nA(3)) ~= 0
            d=d+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G11d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m12*nA(1)+m22*nA(2)+m23*nA(3)) ~= 0
            d=d+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G21d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m13*nA(1)+m23*nA(2)+m33*nA(3)) ~= 0
            d=d+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G31d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m11*nB(1)+m12*nB(2)+m13*nB(3)) ~= 0
            d=d+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G11d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m12*nB(1)+m22*nB(2)+m23*nB(3)) ~= 0
            d=d+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G21d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m13*nB(1)+m23*nB(2)+m33*nB(3)) ~= 0
            d=d+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G31d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m11*nC(1)+m12*nC(2)+m13*nC(3)) ~= 0
            d=d+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G11d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m12*nC(1)+m22*nC(2)+m23*nC(3)) ~= 0
            d=d+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G21d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m13*nC(1)+m23*nC(2)+m33*nC(3)) ~= 0
            d=d+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G31d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
    end

    function d = IU13(u,v)
        % function IU13 is the integrand for displacement gradient u1,3
        d=zeros(size(x1));
        if (m11*nD(1)+m12*nD(2)+m13*nD(3)) ~= 0
            d=d+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G11d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m12*nD(1)+m22*nD(2)+m23*nD(3)) ~= 0
            d=d+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G21d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m13*nD(1)+m23*nD(2)+m33*nD(3)) ~= 0
            d=d+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G31d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m11*nA(1)+m12*nA(2)+m13*nA(3)) ~= 0
            d=d+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G11d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m12*nA(1)+m22*nA(2)+m23*nA(3)) ~= 0
            d=d+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G21d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m13*nA(1)+m23*nA(2)+m33*nA(3)) ~= 0
            d=d+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G31d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m11*nB(1)+m12*nB(2)+m13*nB(3)) ~= 0
            d=d+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G11d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m12*nB(1)+m22*nB(2)+m23*nB(3)) ~= 0
            d=d+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G21d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m13*nB(1)+m23*nB(2)+m33*nB(3)) ~= 0
            d=d+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G31d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m11*nC(1)+m12*nC(2)+m13*nC(3)) ~= 0
            d=d+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G11d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m12*nC(1)+m22*nC(2)+m23*nC(3)) ~= 0
            d=d+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G21d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m13*nC(1)+m23*nC(2)+m33*nC(3)) ~= 0
            d=d+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G31d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
    end

    function d = IU21(u,v)
        % function IU21 is the integrand for displacement gradient u2,1
        d=zeros(size(x2));
        if (m11*nD(1)+m12*nD(2)+m13*nD(3)) ~= 0
            d=d+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G12d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m12*nD(1)+m22*nD(2)+m23*nD(3)) ~= 0
            d=d+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G22d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m13*nD(1)+m23*nD(2)+m33*nD(3)) ~= 0
            d=d+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G32d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m11*nA(1)+m12*nA(2)+m13*nA(3)) ~= 0
            d=d+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G12d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m12*nA(1)+m22*nA(2)+m23*nA(3)) ~= 0
            d=d+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G22d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m13*nA(1)+m23*nA(2)+m33*nA(3)) ~= 0
            d=d+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G32d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m11*nB(1)+m12*nB(2)+m13*nB(3)) ~= 0
            d=d+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G12d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m12*nB(1)+m22*nB(2)+m23*nB(3)) ~= 0
            d=d+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G22d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m13*nB(1)+m23*nB(2)+m33*nB(3)) ~= 0
            d=d+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G32d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m11*nC(1)+m12*nC(2)+m13*nC(3)) ~= 0
            d=d+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G12d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m12*nC(1)+m22*nC(2)+m23*nC(3)) ~= 0
            d=d+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G22d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m13*nC(1)+m23*nC(2)+m33*nC(3)) ~= 0
            d=d+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G32d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
    end

    function d = IU22(u,v)
        % function IU22 is the integrand for displacement gradient u2,2
        d=zeros(size(x2));
        if (m11*nD(1)+m12*nD(2)+m13*nD(3)) ~= 0
            d=d+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G12d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m12*nD(1)+m22*nD(2)+m23*nD(3)) ~= 0
            d=d+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G22d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m13*nD(1)+m23*nD(2)+m33*nD(3)) ~= 0
            d=d+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G32d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m11*nA(1)+m12*nA(2)+m13*nA(3)) ~= 0
            d=d+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G12d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m12*nA(1)+m22*nA(2)+m23*nA(3)) ~= 0
            d=d+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G22d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m13*nA(1)+m23*nA(2)+m33*nA(3)) ~= 0
            d=d+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G32d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m11*nB(1)+m12*nB(2)+m13*nB(3)) ~= 0
            d=d+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G12d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m12*nB(1)+m22*nB(2)+m23*nB(3)) ~= 0
            d=d+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G22d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m13*nB(1)+m23*nB(2)+m33*nB(3)) ~= 0
            d=d+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G32d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m11*nC(1)+m12*nC(2)+m13*nC(3)) ~= 0
            d=d+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G12d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m12*nC(1)+m22*nC(2)+m23*nC(3)) ~= 0
            d=d+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G22d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m13*nC(1)+m23*nC(2)+m33*nC(3)) ~= 0
            d=d+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G32d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
    end

    function d = IU23(u,v)
        % function IU23 is the integrand for displacement gradient u2,3
        d=zeros(size(x2));
        if (m11*nD(1)+m12*nD(2)+m13*nD(3)) ~= 0
            d=d+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G12d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m12*nD(1)+m22*nD(2)+m23*nD(3)) ~= 0
            d=d+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G22d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m13*nD(1)+m23*nD(2)+m33*nD(3)) ~= 0
            d=d+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G32d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m11*nA(1)+m12*nA(2)+m13*nA(3)) ~= 0
            d=d+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G12d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m12*nA(1)+m22*nA(2)+m23*nA(3)) ~= 0
            d=d+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G22d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m13*nA(1)+m23*nA(2)+m33*nA(3)) ~= 0
            d=d+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G32d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m11*nB(1)+m12*nB(2)+m13*nB(3)) ~= 0
            d=d+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G12d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m12*nB(1)+m22*nB(2)+m23*nB(3)) ~= 0
            d=d+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G22d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m13*nB(1)+m23*nB(2)+m33*nB(3)) ~= 0
            d=d+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G32d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m11*nC(1)+m12*nC(2)+m13*nC(3)) ~= 0
            d=d+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G12d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m12*nC(1)+m22*nC(2)+m23*nC(3)) ~= 0
            d=d+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G22d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m13*nC(1)+m23*nC(2)+m33*nC(3)) ~= 0
            d=d+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G32d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
    end

    function d = IU31(u,v)
        % function IU31 is the integrand for displacement gradient u3,1
        d=zeros(size(x3));
        if (m11*nD(1)+m12*nD(2)+m13*nD(3)) ~= 0
            d=d+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G13d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m12*nD(1)+m22*nD(2)+m23*nD(3)) ~= 0
            d=d+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G23d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m13*nD(1)+m23*nD(2)+m33*nD(3)) ~= 0
            d=d+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G33d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m11*nA(1)+m12*nA(2)+m13*nA(3)) ~= 0
            d=d+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G13d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m12*nA(1)+m22*nA(2)+m23*nA(3)) ~= 0
            d=d+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G23d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m13*nA(1)+m23*nA(2)+m33*nA(3)) ~= 0
            d=d+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G33d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m11*nB(1)+m12*nB(2)+m13*nB(3)) ~= 0
            d=d+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G13d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m12*nB(1)+m22*nB(2)+m23*nB(3)) ~= 0
            d=d+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G23d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m13*nB(1)+m23*nB(2)+m33*nB(3)) ~= 0
            d=d+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G33d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m11*nC(1)+m12*nC(2)+m13*nC(3)) ~= 0
            d=d+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G13d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m12*nC(1)+m22*nC(2)+m23*nC(3)) ~= 0
            d=d+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G23d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m13*nC(1)+m23*nC(2)+m33*nC(3)) ~= 0
            d=d+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G33d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
    end

    function d = IU32(u,v)
        % function IU32 is the integrand for displacement gradient u3,2
        d=zeros(size(x3));
        if (m11*nD(1)+m12*nD(2)+m13*nD(3)) ~= 0
            d=d+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G13d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m12*nD(1)+m22*nD(2)+m23*nD(3)) ~= 0
            d=d+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G23d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m13*nD(1)+m23*nD(2)+m33*nD(3)) ~= 0
            d=d+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G33d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m11*nA(1)+m12*nA(2)+m13*nA(3)) ~= 0
            d=d+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G13d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m12*nA(1)+m22*nA(2)+m23*nA(3)) ~= 0
            d=d+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G23d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m13*nA(1)+m23*nA(2)+m33*nA(3)) ~= 0
            d=d+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G33d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m11*nB(1)+m12*nB(2)+m13*nB(3)) ~= 0
            d=d+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G13d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m12*nB(1)+m22*nB(2)+m23*nB(3)) ~= 0
            d=d+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G23d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m13*nB(1)+m23*nB(2)+m33*nB(3)) ~= 0
            d=d+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G33d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m11*nC(1)+m12*nC(2)+m13*nC(3)) ~= 0
            d=d+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G13d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m12*nC(1)+m22*nC(2)+m23*nC(3)) ~= 0
            d=d+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G23d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m13*nC(1)+m23*nC(2)+m33*nC(3)) ~= 0
            d=d+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G33d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
    end

    function d = IU33(u,v)
        % function IU33 is the integrand for displacement gradient u3,3
        d=zeros(size(x3));
        if (m11*nD(1)+m12*nD(2)+m13*nD(3)) ~= 0
            d=d+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G13d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m12*nD(1)+m22*nD(2)+m23*nD(3)) ~= 0
            d=d+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G23d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m13*nD(1)+m23*nD(2)+m33*nD(3)) ~= 0
            d=d+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G33d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if (m11*nA(1)+m12*nA(2)+m13*nA(3)) ~= 0
            d=d+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G13d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m12*nA(1)+m22*nA(2)+m23*nA(3)) ~= 0
            d=d+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G23d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m13*nA(1)+m23*nA(2)+m33*nA(3)) ~= 0
            d=d+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G33d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)));
        end
        if (m11*nB(1)+m12*nB(2)+m13*nB(3)) ~= 0
            d=d+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G13d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m12*nB(1)+m22*nB(2)+m23*nB(3)) ~= 0
            d=d+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G23d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m13*nB(1)+m23*nB(2)+m33*nB(3)) ~= 0
            d=d+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G33d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)));
        end
        if (m11*nC(1)+m12*nC(2)+m13*nC(3)) ~= 0
            d=d+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G13d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m12*nC(1)+m22*nC(2)+m23*nC(3)) ~= 0
            d=d+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G23d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
        if (m13*nC(1)+m23*nC(2)+m33*nC(3)) ~= 0
            d=d+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G33d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)));
        end
    end

end

function y=heaviside(x)
        y=x>0;
end

function p=validateN(x)
if ~(x > 0)
    error('MATLAB:invalid','invalid number of integration points');
end
p = true;
end

function [xn,wn]=gaussxw(a,b,n)
% GAUSSXW finds sample points xn and weights wn for Gaussian
% quadrature on (a,b) with n nodes
%
%    function [xn wn]  = gaussxw(a,b, n)
%
%  Integral = wn(:)' * f (xn(:))
%  or  sum(wn.*f(xn))
%
%  Saves nodes and weights from previous call with same n.
%
% AUTHOR: Trefethen, Spectral Method in Matlab

persistent N x w

% check if we need new x, w
if (isempty(N) == 1 || N ~= n)
    N=n;
    beta=0.5./sqrt(1-(2*(1:N-1)).^(-2));
    T=diag(beta,1)+diag(beta,-1);
    [V,D]=eig(T);
    x=diag(D);
    [x,i]=sort(x);
    w=2*V(1,i).^2;
end

% finds sample x and weights for interval (a,b)
xn=(a+b+x(:)*(b-a))/2;
wn=(b-a)*w(:)/2;
end

