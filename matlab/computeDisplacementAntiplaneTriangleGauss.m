function u1=computeDisplacementAntiplaneTriangleGauss( ...
    x2,x3,A,B,C,e12,e13)
% function COMPUTEDISPLACEMENTANTIPLANETRIANGLEGAUSS computes
% the displacement field associated with deforming triangle volume element
% considering the following geometry using the Gauss quadrature.
%
%              surface
%      -------------+-------------- E (x2)
%                   |
%                   |     + A
%                   |    /  .
%                   |   /     .
%                   |  /        .
%                   | /           .
%                   |/              + B
%                   /            .
%                  /|          /  
%                 / :       .
%                /  |    /
%               /   : .
%              /   /|
%             / .   :
%            +      |
%          C        :
%                   |
%                   D (x3)
%
% Input:
% x2, x3             east coordinates and depth of the observation point,
% A, B, C            east and depth coordinates of the vertices,
% e12, e13           source strain component 12 and 13 in the volume element.
%
% Output:
% u1                 displacement component in the north (along-strike)
%                    direction.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - April 7, 2018, Los Angeles.

assert(min(x3(:))>=0,'depth must be positive.');

% unit vectors
nA = [C(2)-B(2);
      B(1)-C(1)]/norm(C-B);
nB = [C(2)-A(2);
      A(1)-C(1)]/norm(C-A);
nC = [B(2)-A(2);
      A(1)-B(1)]/norm(B-A);
  
% check that unit vectors are pointing outward
if (nA'*(A(:)-(B(:)+C(:))/2))>0
    nA=-nA;
end
if (nB'*(B(:)-(A(:)+C(:))/2))>0
    nB=-nB;
end
if (nC'*(C(:)-(A(:)+B(:))/2))>0
    nC=-nC;
end

% parameterized line integral
y2=@(t,A,B) (A(1)+B(1))/2+t*(B(1)-A(1))/2;
y3=@(t,A,B) (A(2)+B(2))/2+t*(B(2)-A(2))/2;

% Green's functions
G11=@(y2,y3) -1/4/pi*(log((x2-y2).^2+(x3-y3).^2)+log((x2-y2).^2+(x3+y3).^2));

% function IU1 is the integrand for displacement component u1
% (note that 2*e12 and R/2 cancel the factors of two.)
IU1=@(t) ...
    (e12*nC(1)+e13*nC(2))*norm(B-A)*G11(y2(t,A,B),y3(t,A,B)) ...
   +(e12*nA(1)+e13*nA(2))*norm(C-B)*G11(y2(t,B,C),y3(t,B,C)) ...
   +(e12*nB(1)+e13*nB(2))*norm(A-C)*G11(y2(t,C,A),y3(t,C,A));

% numerical solution with Gauss-Legendre quadrature
[sk,gk]=gaussxw(-1,1,15);

u1=zeros(size(x2));
for k=1:length(sk)
    u1=u1+gk(k)*IU1(sk(k));
end

end

function [xn,wn]=gaussxw(a,b,n)
% GAUSSXW finds sample points xn and weights wn for Gaussian
% quadrature on (a,b) with n nodes
%
%    function [xn wn] = gaussxw(a,b, n)
%
%  Integral = wn(:)' * f (xn(:))
%  or  sum(wn.*f(xn))
%
%  Saves nodes and weights from previous call with same n.
%
% AUTHOR: Trefethen, Spectral Method in Matlab

persistent N x w

% check if we need new x, w
if (isempty(N) == 1 || N ~= n)
    N=n;
    beta=0.5./sqrt(1-(2*(1:N-1)).^(-2));
    T=diag(beta,1)+diag(beta,-1);
    [V,D]=eig(T);
    x=diag(D);
    [x,i]=sort(x);
    w=2*V(1,i).^2;
end

% finds sample x and weights for interval (a,b)
xn=(a+b+x(:)*(b-a))/2;
wn=(b-a)*w(:)/2;
end


