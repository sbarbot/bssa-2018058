# README #

This repository contains a suite of analytical and semi-analytical solutions for the displacements, 
strains, and stress due to distributed anelastic deformation of finite strain volumes in a half‐space
for tetrahedral sources. The same is available for triangular source for two-dimensional problems. 
The solutions are provided for the cases of antiplane strain, plane strain, and 3D deformation. These 
expressions represent powerful tools for the analysis of deformation data to image distributed strain 
in the Earth’s interior and for the dynamic modeling of distributed deformation, and to conform meshes
to structural data. 

### Publications

Barbot, Sylvain. "Asthenosphere flow modulated by megathrust earthquake cycles." 
Geophysical Research Letters (2018), doi: 10.1029/2018GL078197.

Barbot, Sylvain. "Deformation of a Half‐Space from Anelastic Strain Confined in a Tetrahedral Volume." 
Bulletin of the Seismological Society of America (2018), doi: 10.1785/0120180058.

### What is this repository for? ###

This repository is to provide scientific computing libraries to evaluate the displacement and stress
caused by distributed anelastic deformation confined in tetrahedral volumes. The kernels allow for 
linear inversion of deformation data for distributed deformation in the crust or mantle, or dynamic 
simulations of viscoelastic and poroelastic behavior using the integral method.
